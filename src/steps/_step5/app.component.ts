import { Component } from '@angular/core';
import { doesNotReject } from 'assert';

// One Service
export class OneService {
  doOne(){}
}
// Two Service
export class TwoService {
  doTwo(){}
}
// Three Service
export class ThreeService {
  doThree(){}
}
// Four Service
export class FourService {
  doFour(){}
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ld-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  greeting = 'Hello';
  constructor(oneService: OneService,
              twoService: TwoService,
              threeService: ThreeService,
              fourService: FourService) {
  }
  getWelcomingMessage(userName: string): string {
    return `${this.greeting} ${userName}`;
  }
}
