import { AppComponent, FourService, OneService, ThreeService, TwoService} from './app.component';
import { TestBed } from '@angular/core/testing';
describe('METHOD: getWelcomingMessage', () => {

  // Injection
  let appComponentTest: AppComponent;

  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue;
  let fakeUserName: string;
  Given(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        OneService,
        TwoService,
        ThreeService,
        FourService,
        AppComponent
      ],
      declarations: []
    });
    appComponentTest = TestBed.get(AppComponent);
  });

  When(() => {
    actualValue = appComponentTest.getWelcomingMessage(fakeUserName);
  });

  describe('user name is Bonnie', () => {

    Given(() => {
      appComponentTest.greeting = 'Hola';
      fakeUserName = 'Bonnie';
    });

    Then(() => {
      expectedValue = 'Hola Bonnie';
      expect(actualValue).toEqual(expectedValue);
    });
  });

  describe('user name is Alyssa', () => {

    Given(() => {
      fakeUserName = 'Alyssa';
    });

    Then(() => {
      expectedValue = 'Hello Alyssa';
      expect(actualValue).toEqual(expectedValue);
    });
  });
});
