import { TestBed } from '@angular/core/testing';
import { FrontComponent } from './front.component';

describe('Front Component', () => {
  // VARIABLES
  let actualValue: any;
  // INJECTION
  let frontComponentTest: FrontComponent;
  Given(() => {
      console.log('Injection TestBed');
      TestBed.configureTestingModule({
        providers: [FrontComponent]
      });
      frontComponentTest = TestBed.get(FrontComponent);
  });
   // ACTION
  When(() => {
    console.log('When Action');
    actualValue = frontComponentTest.isListVisible();
  });

  // TEST isListVisible false
  describe('Test fonction isListVisible false', () => {
    // INPUT
    Given(() => {
      console.log('Given TEST isListVisible false');
      frontComponentTest.llamas = [];
    });
    // OUTPUT
    Then(() => {
      console.log('Then TEST isListVisible false');
      expect(actualValue).toEqual(false);
    });
  });
  // TEST isListVisible true
  describe('Test fonction isListVisible true', () => {
    // INPUT
    Given(() => {
      console.log('Given TEST isListVisible true');
      frontComponentTest.llamas = [
        {name: 'Bonnie'}
      ];
    });
    // OUTPUT
    Then(() => {
      console.log('Then TEST isListVisible true');
      expect(actualValue).toEqual(true);
    });
  });
});
