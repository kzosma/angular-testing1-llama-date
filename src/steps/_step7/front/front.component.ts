import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ld-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.css']
})
export class FrontComponent {

  llamas = [
    { name: 'Richard' },
    { name: 'Bonnie' }
  ];

  isListVisible(): boolean {
    return this.llamas.length > 0;
  }
}
