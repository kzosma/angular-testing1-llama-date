import { TestBed } from '@angular/core/testing';
import { LlamaComponent } from './llama.component';

describe('METHOD: speak', () => {

  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue;
  let fakeWord: string;
  let componentUnderTest: LlamaComponent;

  Given(() => {
    TestBed.configureTestingModule({
        providers: [LlamaComponent]
    });
    componentUnderTest = TestBed.get(LlamaComponent);

  });

  When(() => {
    actualValue = componentUnderTest.speak(fakeWord);
  });

  describe('user name is Bonnie', () => {
    Given(() => {
      fakeWord = 'Bonnie';
    });

    Then(() => {
      expectedValue = 'Llama says: Bonnie';
      expect(actualValue).toEqual(expectedValue);
    });
  });

});
