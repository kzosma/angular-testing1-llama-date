import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ld-llama',
  template: `<div> {{ speak('baaa') }} </div>`
})
export class LlamaComponent {
  speak(word: string): string {
    return `Llama says: ${word}`;
  }
}
