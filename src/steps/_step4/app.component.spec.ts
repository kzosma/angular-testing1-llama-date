describe('Tests Suite for getWelcomingMessage', () => {

  // Function
  function getWelcomingMessage(userName: string): string {
    return `Hello ${userName}`;
  }

  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue, fakeValue;

  // WHEN
  When(() => {
    console.log('WHEN');
    actualValue = getWelcomingMessage(fakeValue);
  });

  // GIVEN, WHEN, THEN :  Test Suite 1
  describe('should return hello with Bonnie', () => {
    // GIVEN
    Given(() => {
      console.log('GIVEN 1');
      fakeValue = 'Bonnie';
    });
    // THEN
    Then(() => {
      console.log('THEN 1');
      expectedValue = 'Hello Bonnie';
      expect(actualValue).toEqual(expectedValue);
    });

  });
  // GIVEN, WHEN, THEN :  Test Suite 2
  describe('should return hello with Alyssa', () => {
  // GIVEN
  Given(() => {
    console.log('GIVEN 2');
    fakeValue = 'Alyssa';
  });
  // THEN
  Then(() => {
    console.log('THEN 2');
    expectedValue = 'Hello Alyssa';
    expect(actualValue).toEqual(expectedValue);
  });
  });
});
