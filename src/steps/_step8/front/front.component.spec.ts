import { TestBed } from '@angular/core/testing';
import { assert } from 'console';
import { FrontComponent } from './front.component';
describe('Front Component', () => {
  let frontComponentTest: FrontComponent;
  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue;
  // BeforeEach
  Given(() => {
    TestBed.configureTestingModule({
      providers: [FrontComponent]
    });
    frontComponentTest = TestBed.get(FrontComponent);
  });
  // Action
  When(() => {
    actualValue = frontComponentTest.isListVisible();
  });
  // First Test
  describe('isListVisible true', () => {
    // BeforeEach
    Given(() => {
      frontComponentTest.llamas = [{name: 'Bonnie'}];
    });
    Then(() => {
      expectedValue = true;
      expect(actualValue).toEqual(expectedValue);
    });
  });
  // Second Test
  describe('isListVisible false', () => {
    // BeforeEach
    Given(() => {
      frontComponentTest.llamas = [];
    });
    Then(() => {
      expectedValue = false;
      expect(actualValue).toEqual(expectedValue);
    });
  });
  // Third Test
  describe('isListVisible undefined', () => {
    // BeforeEach
    Given(() => {
      frontComponentTest.llamas = undefined;
    });
    Then(() => {
      expectedValue = false;
      expect(actualValue).toEqual(expectedValue);
    });
  });
});

