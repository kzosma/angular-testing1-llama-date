describe('Welcome Message Tests Suite', () => {
  function getWelcomeMessage(message: string) {
    return `Hello ${message}`;
  }
  // Before
  beforeEach(() => {
    console.log('Before Each Test');
  });
  // After
  afterEach(() => {
    console.log('After Each Test');
  });

  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue;
  // IT 1
  it('Test Welcome Message Bonnie', () => {
    actualValue = getWelcomeMessage('Bonnie');
    expectedValue = 'Hello Bonnie';
    console.log('Test Welcome Message Bonnie');
    expect( actualValue ).toEqual( expectedValue );
  });
  // IT 2
  it('Test Welcome Message Alyssa', () => {
    actualValue = getWelcomeMessage('Alyssa');
    expectedValue = 'Hello Alyssa';
    console.log('Test Welcome Message Alyssa');
    expect( actualValue ).toEqual( expectedValue );
  });
  // Sub Describe
  describe('Sub describe', () => {
    it('should fail', () => {
      console.log('Test Sub Describe > should fail');
      expect(true).toEqual(true);
    });
  });

});


