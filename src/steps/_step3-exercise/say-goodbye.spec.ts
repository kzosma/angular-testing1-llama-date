describe('Exercise', () => {
  // Function
  function sayGoodbye(name?: string): string {
    if (!name) {
      return 'No Name!';
    }
    return 'Goodbye ' + name + '!';
  }
  // Before Each
  beforeEach(() => {
    console.log('I LOVE MY LLAMAS!');
  });

  // tslint:disable-next-line:one-variable-per-declaration
  let actualValue, expectedValue ;
  // IT 1
  it('should check sayGoodbye function with Bonnie', () => {

    actualValue = sayGoodbye('Bonnie');
    expectedValue = 'Goodbye Bonnie!';
    expect(actualValue).toEqual(expectedValue);

  });
  describe('Nested Group', () => {
    beforeEach(() => {
      console.log('I LOVE MY LLAMAS IT 2!');
    });
      // IT 2
    it('should check sayGoodbye function with no input', () => {
      actualValue = sayGoodbye();
      expectedValue = 'No Name!';
      expect(actualValue).toEqual(expectedValue);

    });
  });

});
