// 1) describe a deux paramètres :
// - String pour décrire la suite de tests
// - fonction callback
// 2) it : fonction représentant un test spécifique
// - String : Description du test
// - fonction callback
// 3) expect : fonction d'assertion
describe('Dummy Test Suite', () => {
  it('dummy test', () => {
    expect(true).toBe(true);
  });
});
